﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Animator startButton;
    public Animator settingsButton;
    public Animator dialog;
    public Animator contentPanel;
    public Animator gearImage;
    public Animator PurchaseButton;
    public Animator PurchaseDialog;


    // Start is called before the first frame update
    public void StartGame()
    {
        SceneManager.LoadScene("RocketMouse");
    }
    public void OpenSettings()
    {
        startButton.SetBool("isHidden", true);
        settingsButton.SetBool("isHidden", true);
        dialog.SetBool("isHidden", false);
    }
    public void CloseSettings()
    {
        startButton.SetBool("isHidden", false);
        settingsButton.SetBool("isHidden", false);
        dialog.SetBool("isHidden", true);
    }
    public void ToggleMenu()
    {
        bool isHidden = contentPanel.GetBool("isHidden");
        contentPanel.SetBool("isHidden", !isHidden);
        gearImage.SetBool("isHidden", !isHidden);
    }
    public void OpenPurchase()
    {
        startButton.SetBool("isHidden", true);
        settingsButton.SetBool("isHidden", false);
        PurchaseButton.SetBool("isHidden", true);
        PurchaseDialog.SetBool("isHidden", false);
    }
    public void Closepurchase()
    {
        startButton.SetBool("isHidden", false);
        settingsButton.SetBool("isHidden", true);
        PurchaseButton.SetBool("isHidden", false);
        PurchaseDialog.SetBool("isHidden", true);
    }
}
